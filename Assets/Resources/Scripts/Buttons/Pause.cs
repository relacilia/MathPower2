﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Pause : MonoBehaviour {

    bool paused = false;
    private Button myselfButton;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        myselfButton = GetComponent<Button>();
        myselfButton.onClick.AddListener(() => togglePause());
        Debug.Log(Time.timeScale);
    }

    bool togglePause()
    {
        if (Time.timeScale != 0)
        {
            Time.timeScale = 0;
            return (false);
        }
        else
        {
            Time.timeScale = 1;
            return (true);
        }
    }
}
