﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Next : MonoBehaviour {

    ActionControl action = new ActionControl();
    private Button myselfButton;

    // Use this for initialization
    void Start () {
        int level1 = PlayerPrefs.GetInt("jenisLevel");
        int sublevel = PlayerPrefs.GetInt("levelPlay");
        myselfButton = GetComponent<Button>();
        myselfButton.onClick.AddListener(() => Playnext(level1, sublevel));
    }

    public void Playnext(int level1 , int sublevel)
    {
        int nextLevel = sublevel + 1;
        if (level1 == 0 && nextLevel < 10)
        {
            action.EasyPlayButton(nextLevel);
        }
        else if (level1 == 0 && nextLevel == 10)
        {
            action.MediumPlayButton(0);
        }
        else if (level1 == 1 && nextLevel < 10)
        {
            action.MediumPlayButton(nextLevel);
        }
        else if (level1 == 1 && nextLevel == 10)
        {
            action.HardPlayButton(0);
        }
        else if (level1 == 2 && nextLevel < 10)
        {
            action.HardPlayButton(nextLevel);
        }
        else
        {
            action.HardPlayButton(9);
        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
