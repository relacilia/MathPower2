﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HomeLevel : MonoBehaviour {

    ActionControl action = new ActionControl();
    private Button myselfButton;

    // Use this for initialization
    void Start () {
        int level1 = PlayerPrefs.GetInt("jenisLevel");
        myselfButton = GetComponent<Button>();
        myselfButton.onClick.AddListener(() => GoLevel(level1));
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void GoLevel(int level1)
    {
        if (level1 == 0)
        {
            action.Easy();
        }
        else if (level1 == 1)
        {
            action.Medium();
        }
        else
        {
            action.Hard();
        }
    }
}
