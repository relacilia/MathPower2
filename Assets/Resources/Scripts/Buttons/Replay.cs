﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Replay : MonoBehaviour {

    ActionControl action = new ActionControl();
    private Button myselfButton;

    // Use this for initialization
    void Start () {
        int level1 = PlayerPrefs.GetInt("jenisLevel");
        int sublevel = PlayerPrefs.GetInt("levelPlay");
        myselfButton = GetComponent<Button>();
        myselfButton.onClick.AddListener(() => Replaygame (level1, sublevel));

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Replaygame (int level1, int sublevel)
    {
        if (level1 == 0)
        {
            action.EasyPlayButton(sublevel);
        }
        else if (level1 == 1)
        {
            action.MediumPlayButton(sublevel);
        }
        else
        {
            action.HardPlayButton(sublevel);
        }
    }
}
