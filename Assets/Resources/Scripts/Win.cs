﻿using UnityEngine;
using System.Collections;

public class Win : MonoBehaviour {

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void createWin()
    {
        GameObject item = (GameObject)Instantiate(Resources.Load<GameObject>("Prefabs/Win"), this.transform.position, Quaternion.identity);
        item.transform.SetParent(this.transform);
        item.transform.localScale = new Vector3(1, 1, 1);
        item.GetComponent<AudioSource>().Play();
    }

    public void createLose()
    {
        GameObject item = (GameObject)Instantiate(Resources.Load<GameObject>("Prefabs/Lose"), this.transform.position, Quaternion.identity);
        item.transform.SetParent(this.transform);
        item.transform.localScale = new Vector3(1, 1, 1);
        item.GetComponent<AudioSource>().Play();
    }

    public void createPause()
    {
        GameObject item = (GameObject)Instantiate(Resources.Load<GameObject>("Prefabs/Pause"), this.transform.position, Quaternion.identity);
        item.transform.SetParent(this.transform);
        item.transform.localScale = new Vector3(1, 1, 1);
        item.transform.localPosition = new Vector3(0, -224, 0);
    }

}
