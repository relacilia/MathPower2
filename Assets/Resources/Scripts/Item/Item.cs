﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Item : MonoBehaviour {
    int price;
    GameObject gamePlay;

	// Use this for initialization
    public int Price
    {
        get
        {
            return price;
        }
        set
        {
            price = value;
            transform.GetChild(1).GetComponent<Text>().text = price.ToString();
        }
    }

	void Start () {
        gamePlay = GameObject.Find("GamePlay");
        gamePlay.GetComponent<GamePlay>().items.Add(Price);
        gamePlay.GetComponent<GamePlay>().gameObjectItems.Add(this.gameObject);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
