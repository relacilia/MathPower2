﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Buttonlevel : MonoBehaviour {
    int nolevel;
    GameObject gamePlay;
    Button levelButton;
    ActionControl ac = new ActionControl();

    public int Number
    {
        get
        {
            return nolevel;
        }
        set
        {
            nolevel = value;
            transform.GetChild(0).GetComponent<Text>().text = nolevel.ToString();
        }
    }
    // Use this for initialization
    void Start () {
        levelButton = GetComponent<Button>();
        levelButton.onClick.AddListener(() => load());
    }
	
	// Update is called once per frame
	void Update () {
	    
	}

    void load ()
    {
        Debug.Log(PlayerPrefs.GetInt("jenisLevel2"));
        if (PlayerPrefs.GetInt("jenisLevel2")==0)
        { ac.EasyPlayButton(nolevel - 1); }
        else if (PlayerPrefs.GetInt("jenisLevel2") == 1) 
        { ac.MediumPlayButton(nolevel - 1); }
        else if (PlayerPrefs.GetInt("jenisLevel2") == 2) 
        { ac.HardPlayButton(nolevel - 1); }

    }
}
