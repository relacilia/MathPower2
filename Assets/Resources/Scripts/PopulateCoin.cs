﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;
public class PopulateCoin : MonoBehaviour {

    int MAX_COIN = 24;

    void Start () {
        for (int i = 0; i< MAX_COIN; i++)
        {
            createCoin();
        }
    }

    public void createCoin(int siblingIndex = -1, string Operator = null)
    {
        GameObject coin = (GameObject)Instantiate(Resources.Load<GameObject>("Prefabs/coin"), this.transform.position, Quaternion.identity);
        coin.transform.SetParent(this.transform);
        coin.transform.localScale = new Vector3(1, 1, 1);
        coin.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprite/coins");
        
        if (siblingIndex !=-1)
        {
            coin.transform.SetSiblingIndex(siblingIndex);
        }


        if(Operator == "Multiple" || Operator == "Div")
            coin.GetComponent<Coin>().MathValue = Random.Range(2, 3);
        else if (Operator == "Minus")
            coin.GetComponent<Coin>().MathValue = Random.Range(1, 5);
        else
            coin.GetComponent<Coin>().MathValue = Random.Range(1, 9);

        if (Operator != null)
        {
            coin.GetComponent<Coin>().MathOperator = Operator;
        } else
        {
            coin.GetComponent<Coin>().MathOperator = "Plus";
        }   
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
