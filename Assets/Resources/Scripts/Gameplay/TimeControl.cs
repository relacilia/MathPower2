﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeControl : MonoBehaviour {

    float timeLeft;

    // Use this for initialization
    void Start () {
        timeLeft = 100.0f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate()
    {
        timeLeft -= Time.deltaTime;

        if(timeLeft > 0)
        {
            this.transform.GetChild(0).GetComponent<Image>().fillAmount = (float)this.timeLeft / 100.0f;
        }

    }
}
