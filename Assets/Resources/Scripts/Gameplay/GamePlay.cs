﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class GamePlay : MonoBehaviour {
    Level level = new Level();

    int playerCoinValue;
    int healt = 2;
    int ular;
    

    GameObject currentPlayerCoin;
    GameObject PlayerMonster;
    GameObject EnemyMonster;
    GameObject PlayerHealth;
    GameObject jumlahSnake;
    GameObject pause;

    
    

    public List<int> items;
    public List<GameObject> gameObjectItems;

    GameObject item;

    public int JumlahUlar
    {
        get
        {
            return ular;
        }
        set
        {
            ular = value;
            jumlahSnake.transform.GetChild(0).GetComponent<Text>().text = ular.ToString();
        }
    }

    public int HealtValue
    {
        get
        {
            return healt;
        }
        set
        {
            healt = value;
        }
    }

    public int PlayerCoinValue
    {
        get
        {
            return playerCoinValue;
        }

        set
        {
            playerCoinValue = value;
            currentPlayerCoin.transform.GetChild(1).GetComponent<Text>().text = playerCoinValue.ToString();
        }
    }

    // Use this for initialization
    void Start () {
        items = new List<int>();
        currentPlayerCoin       = GameObject.Find("PlayerCurrentCoins");
        PlayerCoinValue = 0;
        jumlahSnake = GameObject.Find("Snake");
        JumlahUlar = level.Snake[PlayerPrefs.GetInt("jenisLevel"), PlayerPrefs.GetInt("levelPlay")];
        Time.timeScale = 1;


        PlayerMonster = GameObject.Find("PanelBattle").transform.GetChild(0).gameObject;
        EnemyMonster = GameObject.Find("PanelBattle").transform.GetChild(1).gameObject;
        PlayerHealth = GameObject.Find("PanelPlayerInfo").transform.GetChild(0).gameObject;
    }
	
	// Update is called once per frame
	void Update () {
	    if(healt == 0)
        {

        }
	}

    public void togglePause()
    {
        if (Time.timeScale != 0)
        {
            Time.timeScale = 0;
            createPause();

        }
        else
        {
            Time.timeScale = 1;
            destroyPause();
        }
    }

    public void createPlayerAttack(string nameItem, int valueItem)
    {
        
        GameObject weapon = (GameObject)Instantiate(Resources.Load<GameObject>("Prefabs/Weapon"), this.transform.position, Quaternion.identity);
        weapon.transform.SetParent(PlayerMonster.transform);
        weapon.transform.localPosition = new Vector3(3, 0, 0);
        weapon.GetComponent<Laser>().price = valueItem;
        weapon.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprite/Laser/" + nameItem);
    }

    public void createPause()
    {
        item = (GameObject)Instantiate(Resources.Load<GameObject>("Prefabs/Pause"), this.transform.position, Quaternion.identity);
        pause = GameObject.Find("PanelWinLose");
        item.transform.SetParent(pause.transform);
        item.transform.localScale = new Vector3(1, 1, 1);
        item.transform.localPosition = new Vector3(0, -224, 0);
    }

    public void destroyPause()
    {
        Destroy(item);
    }
}
