﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class LevelTrio : MonoBehaviour {

	// Use this for initialization
	void Start () {

        int level1 = PlayerPrefs.GetInt("jenisLevel");
        int sublevel = PlayerPrefs.GetInt("levelPlay")+1;
        string level2 = biglevel(level1);
        gameObject.transform.GetComponent<Text>().text = level2 + "lv " + sublevel;
        

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public string biglevel (int level1)
    {
        if (level1 == 0)
        {
            return "Easy ";
        }
        else if (level1 == 1)
        {
            return "Medium ";
        }
        else {
            return "Hard ";
        }
    }
}
