﻿using UnityEngine;
using System.Collections;

public class Level : MonoBehaviour {

    public float[,] speed = new float[,] { { 0.08f, 0.1f, 0.12f, 0.14f, 0.14f, 0.16f, 0.18f, 0.18f, 0.2f }, { 0.16f, 0.16f, 0.18f, 0.2f, 0.21f, 0.22f, 0.22f, 0.24f, 0.25f }, { 0.20f, 0.21f, 0.23f, 0.25f, 0.28f, 0.30f, 0.32f, 0.32f, 0.34f } };
    public int[,] HP = new int[,] { { 10, 12, 14, 16, 16, 16, 18, 20, 23 }, { 20, 24, 27, 28, 28, 29, 27, 27, 35 }, { 30, 32, 33, 35, 40, 42, 45, 48, 50 } };
    public int[,] MaxAngka = new int[,] { { 10, 10, 12, 15, 15, 15, 17, 20, 20 }, { 20, 24, 25, 25, 25, 27, 30, 30, 30 }, { 30, 32, 32, 33, 33, 34, 34, 35, 35 } };
    public int[,] MinAngka = new int[,] { { 5, 8, 10, 10, 10, 10, 10, 10, 10, 10 }, { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 }, { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 } };
    public int[,] jenisOperator = new int[,] { { 1, 1, 1, 1, 1, 1, 1, 4, 4, 4}, { 4, 4, 4, 6, 6, 8, 8, 8, 8, 8 }, { 8, 8, 8, 8, 8, 8, 8, 8, 8, 8 } };
    public float[,] FillHealth = new float[,] { { 20.0f, 20.0f, 20.0f, 20.0f, 21.0f, 22.0f, 22.0f, 24.0f, 26.0f }, { 30.0f, 30.0f, 33.0f, 35.0f, 38.0f, 40.0f, 45.0f, 48.0f, 50.0f }, { 45.0f, 50.0f, 52.0f, 55.0f, 58.0f, 58.0f, 60.0f, 60.0f, 65.0f } };
    public int[,] Snake = new int[,] { { 3, 4, 4, 5, 5, 6, 6, 7, 8 }, { 5, 6, 7, 7, 7, 8, 8, 8, 9 }, { 7, 8, 8, 9, 10, 12, 12, 14, 15 } };
    // Use this for initialization
    void Awake () {
        // PlayerPrefs.SetInt("maxLevel", 1);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
