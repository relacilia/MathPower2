﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using System;

public class PopulateLevelMed : MonoBehaviour {
    int MAX_ITEM = 12;
    ActionControl ac = new ActionControl();
    // Use this for initialization
    void Start () {
        for (int i = 1; i <= MAX_ITEM; i++)
        {
            if (PlayerPrefs.GetInt("maxLevelMedium") >= i)
            {
                GameObject item = (GameObject)Instantiate(Resources.Load<GameObject>("Prefabs/level"), this.transform.position, Quaternion.identity);
                item.transform.SetParent(this.transform);
                item.transform.localScale = new Vector3(0.4654561f, 0.4654561f, 0.4654561f);
                item.GetComponent<Buttonlevel>().Number = i;
                item.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprite/Button/GreyBtn");
            }
            else
            {
                GameObject item = (GameObject)Instantiate(Resources.Load<GameObject>("Prefabs/lockbutton"), this.transform.position, Quaternion.identity);
                item.transform.SetParent(this.transform);
                item.transform.localScale = new Vector3(0.4654561f, 0.4654561f, 0.4654561f);
                item.GetComponent<Buttonlevel>().Number = i;
                item.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprite/Button/GreyBtn");
            }

        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
