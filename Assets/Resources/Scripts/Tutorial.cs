﻿using UnityEngine;
using System.Collections;

public class Tutorial : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void createTurorial()
    {
        GameObject item = (GameObject)Instantiate(Resources.Load<GameObject>("Prefabs/Image"), this.transform.position, Quaternion.identity);
        item.transform.SetParent(this.transform);
        item.transform.localScale = new Vector3(1, 1, 1);
    }
}
