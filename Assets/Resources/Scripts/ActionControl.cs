﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;
public class ActionControl : MonoBehaviour {
    

    // Use this for initialization
    void Awake()
    {
        if (PlayerPrefs.GetInt("maxLevelEasy")==0)
            PlayerPrefs.SetInt("maxLevelEasy", 1);
        if (PlayerPrefs.GetInt("maxLevelMedium") == 0)
            PlayerPrefs.SetInt("maxLevelMedium", 1);
        if (PlayerPrefs.GetInt("maxLevelHard") == 0)
            PlayerPrefs.SetInt("maxLevelHard", 1);
    }

    void Start () {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Help()
    {
        SceneManager.LoadScene("Help");
    }

    public void ChooseLevel()
    {
        SceneManager.LoadScene("Level");
    }

    public void Easy()
    {
        PlayerPrefs.SetInt("jenisLevel2", 0);
        Advertisement.Show();
        SceneManager.LoadScene("EasyLevel");
    }

    public void Medium()
    {
        Advertisement.Show();
        PlayerPrefs.SetInt("jenisLevel2", 1);
        Debug.Log(PlayerPrefs.GetInt("jenisLevel2"));
        SceneManager.LoadScene("MediumLevel");
    }

    public void Hard()
    {
        Advertisement.Show();
        PlayerPrefs.SetInt("jenisLevel2", 2);
        SceneManager.LoadScene("HardLevel");
    }


    public void EasyPlayButton(int level)
    {
        PlayerPrefs.SetInt("jenisLevel", 0);
        PlayerPrefs.SetInt("levelPlay", level);
        if(level == 0)
        {
            SceneManager.LoadScene("Tutorial");
        }
        else SceneManager.LoadScene("Gameplay");
    }

    public void PlayGame()
    {
        PlayerPrefs.SetInt("jenisLevel", 0);
        PlayerPrefs.SetInt("levelPlay", 0);
        SceneManager.LoadScene("Gameplay");
    }

    public void MediumPlayButton(int level)
    {
        PlayerPrefs.SetInt("jenisLevel", 1);
        PlayerPrefs.SetInt("levelPlay", level);
        SceneManager.LoadScene("Gameplay");
    }

    public void HardPlayButton(int level)
    {
        PlayerPrefs.SetInt("jenisLevel", 2);
        PlayerPrefs.SetInt("levelPlay", level);
        SceneManager.LoadScene("Gameplay");
    }
}
