﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
    float speed;
    Level level = new Level();
    GamePlay gamePlay;
    Collider2D temp;
    Win win;
    int triggerWaktu;
    float waktu;
    
    // Use this for initialization
    void Start () {
        speed = level.speed[PlayerPrefs.GetInt("jenisLevel"), PlayerPrefs.GetInt("levelPlay")];
        gamePlay = GameObject.Find("GamePlay").GetComponent<GamePlay>();
        win = GameObject.Find("PanelWinLose").GetComponent<Win>();
        triggerWaktu = 0;
        waktu = 5.0f;

    }  
	
	// Update is called once per frame
	void Update () {
        this.transform.Translate(Vector3.left * Time.deltaTime * speed, Camera.main.transform);
        if (this.triggerWaktu == 1)
        {
            temp.GetComponent<SpriteRenderer>().color = Color.red;
            waktu -= Time.deltaTime;
            if (waktu <= 0)
            {
                this.triggerWaktu = 0;
                temp.GetComponent<SpriteRenderer>().color = Color.white;
            }
        }
	}


    void OnTriggerEnter2D(Collider2D collide)
    {
       
        if(collide.tag == "Player")
        {
            temp = collide;
            this.triggerWaktu = 1;
            int healtplayer = gamePlay.HealtValue;
            Destroy(this.gameObject);
            if (healtplayer == 2)
            {
                Destroy(GameObject.Find("Health").transform.GetChild(2).gameObject);
                gamePlay.HealtValue -= 1;
            }
            else if (healtplayer == 1)
            {
                Destroy(GameObject.Find("Health").transform.GetChild(1).gameObject);
                gamePlay.HealtValue -= 1;
            }
            else
            {
                Destroy(GameObject.Find("Health").transform.GetChild(0).gameObject);
                win.createLose();
                Time.timeScale = 0;
                GameObject.Find("Canvas").GetComponent<AudioSource>().mute = true;
                GameObject.Find("PanelBattle").transform.GetComponentInChildren<SpriteRenderer>().enabled = false;
                GameObject[] gos = GameObject.FindGameObjectsWithTag("Enemy");
                foreach (GameObject go in gos)
                {
                    go.SetActive(false);

                }
            }
        }
        
    }
}
