﻿using UnityEngine;
using System.Collections;

public class Heart : MonoBehaviour {
    int MAX_HEART = 3;
    int x = -358;
    int y = 5;
    int z = 0;
    // Use this for initialization

    void Start () {
        for (int i = 0; i < MAX_HEART; i++)
        {
            createHeart();
        }
    }

    public void createHeart()
    {
        GameObject hati = (GameObject)Instantiate(Resources.Load<GameObject>("Prefabs/Heart"), this.transform.position, Quaternion.identity);
        hati.transform.SetParent(this.transform);
        hati.transform.localScale = new Vector3(0.3522075f, 0.3522075f, 0.3522075f);
        hati.transform.localPosition = new Vector3(x, y, z);
        x = x + 108;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
