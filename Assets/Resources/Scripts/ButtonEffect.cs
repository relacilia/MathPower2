﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonEffect : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    float x, y;
    
    public void OnPointerDown(PointerEventData eventData)
    {
        x = transform.localScale.x;
        y = transform.localScale.y;

        transform.localScale = new Vector3(x / 1.1f, y / 1.1f);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        transform.localScale = new Vector3(x, y);
    }
}
