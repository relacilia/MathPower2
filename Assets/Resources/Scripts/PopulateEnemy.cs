﻿using UnityEngine;
using System.Collections;

public class PopulateEnemy : MonoBehaviour {

    float timeSpeed = 0;
    float time = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        timeSpeed -= Time.deltaTime;
        if(timeSpeed <= 0 )
        {
            GameObject enemy = (GameObject)Instantiate(Resources.Load<GameObject>("Prefabs/Enemy"), this.transform.position, Quaternion.identity);
            enemy.transform.SetParent(this.transform);
            enemy.transform.localScale = new Vector3(-52.2f, 52.2f, -52.2f);
            enemy.transform.localPosition = new Vector3(448, -123, -104);
            timeSpeed = Random.Range(15, 17);
        }
    }
}
