﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;

public class PopulateItem : MonoBehaviour {
    
    int MAX_ITEM = 4;
    Level level = new Level();

	// Use this for initialization
	void Start () {
        for (int i = 0; i < MAX_ITEM; i++)
        {
            createItem();
            
        }
    }

    public void createItem (int siblingIndex = -1)
    {
        GameObject gamePlay = GameObject.Find("GamePlay");

        GameObject item = (GameObject)Instantiate(Resources.Load<GameObject>("Prefabs/Item"), this.transform.position, Quaternion.identity);
        item.transform.SetParent(this.transform);
        item.transform.localScale = new Vector3(1, 1, 1);
        
        if (siblingIndex != -1)
        {
            item.transform.SetSiblingIndex(siblingIndex);
        }

        string[] ItemName = new string[] { "gun2", "gun3", "gun4", "gun6", "gun7", "gun8" };

        //int priceToAdd = Random.Range(level.MinAngka[PlayerPrefs.GetInt("jenisLevel"), PlayerPrefs.GetInt("levelPlay")], level.MaxAngka[PlayerPrefs.GetInt("jenisLevel"), PlayerPrefs.GetInt("levelPlay")]);
        int priceToAdd = Random.Range(level.MinAngka[PlayerPrefs.GetInt("jenisLevel"), PlayerPrefs.GetInt("levelPlay")], level.MaxAngka[PlayerPrefs.GetInt("jenisLevel"), PlayerPrefs.GetInt("levelPlay")]);


        //while (gamePlay.GetComponent<GamePlay>().items.Count != 0 && gamePlay.GetComponent<GamePlay>().items.Contains(priceToAdd)){
            //priceToAdd = Random.Range(level.MinAngka[PlayerPrefs.GetInt("jenisLevel"), PlayerPrefs.GetInt("levelPlay")], level.MaxAngka[PlayerPrefs.GetInt("jenisLevel"), PlayerPrefs.GetInt("levelPlay")]);
        //}

        int jenis_item = Random.Range(0, 3);

        item.GetComponent<Item>().Price = priceToAdd;
        item.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprite/Items/" + ItemName[jenis_item]);

    }

	
	// Update is called once per frame
	void Update () {
        
    }
}
