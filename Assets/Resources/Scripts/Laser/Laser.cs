﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Laser : MonoBehaviour {

    private Rigidbody2D rb;
    public float speed;
    public int price;
    Win win;
    GamePlay gameplay;
    Level level = new Level();
    AudioSource laser;
    

    // Use this for initialization
    void Start () {
        win = GameObject.Find("PanelWinLose").GetComponent<Win>();
        gameplay = GameObject.Find("GamePlay").GetComponent<GamePlay>();
    }
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.right * Time.deltaTime, Camera.main.transform);

    }

    void OnTriggerEnter2D(Collider2D collide)
    {
        if (collide.tag == "Enemy")
        {
            collide.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().fillAmount -= (float)price / level.FillHealth[PlayerPrefs.GetInt("jenisLevel"), PlayerPrefs.GetInt("levelPlay")];
            //StartCoroutine(Example(collide));
            Example(collide);
        }
        
    }

    //IEnumerator Example(Collider2D collide)
    void Example(Collider2D collide)
    {
        //collide.transform.GetComponent<SpriteRenderer>().color = Color.red;
        //yield return new WaitForSeconds(0.3f);
        //collide.transform.GetComponent<SpriteRenderer>().color = Color.white;
        if (collide.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().fillAmount == 0)
        {
            DestroyObject(collide.gameObject);
            GameObject ular1 = GameObject.Find("GamePlay");

            GameObject.Find("GamePlay").GetComponent<GamePlay>().JumlahUlar -= 1;
            if (GameObject.Find("GamePlay").GetComponent<GamePlay>().JumlahUlar == 0)
            {
                win.createWin();
                Time.timeScale = 0;
                GameObject.Find("Canvas").GetComponent<AudioSource>().mute = true;
                GameObject.Find("PanelBattle").transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
                GameObject[] gos = GameObject.FindGameObjectsWithTag("Enemy");
                if (PlayerPrefs.GetInt("jenisLevel")==0)
                {
                    int max = PlayerPrefs.GetInt("maxLevelEasy");
                    int levela = PlayerPrefs.GetInt("levelPlay");
                    if (max - 1 == levela)
                    {
                        PlayerPrefs.SetInt("maxLevelEasy", max + 1);
                    }
                }
                else if (PlayerPrefs.GetInt("jenisLevel") == 1)
                {
                    int max = PlayerPrefs.GetInt("maxLevelMedium");
                    int levela = PlayerPrefs.GetInt("levelPlay");
                    if (max - 1 == levela)
                    {
                        PlayerPrefs.SetInt("maxLevelMedium", max + 1);
                    }
                }
                else if (PlayerPrefs.GetInt("jenisLevel") == 2)
                {
                    int max = PlayerPrefs.GetInt("maxLevelHard");
                    int levela = PlayerPrefs.GetInt("levelPlay");
                    if (max - 1 == levela)
                    {
                        PlayerPrefs.SetInt("maxLevelHard", max + 1);
                    }
                }

                    foreach (GameObject go in gos)
                {
                    go.SetActive(false);

                }
                GameObject[] goo = GameObject.FindGameObjectsWithTag("Laser");
                foreach (GameObject go in goo)
                {
                    go.SetActive(false);

                }
            }

        }
        Destroy(this.gameObject);
    }
}
