﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Coin : MonoBehaviour {
    int mathValue;
    string mathOperator;
    Button coinButton;
    GameObject gamePlay;
    GameObject Items;
    Level level = new Level();
    AudioSource clickAudio;

    public int MathValue
    {
        get
        {
            return mathValue;
        }
        set
        {
            mathValue = value;
            transform.GetChild(0).GetComponent<Text>().text = mathValue.ToString();
        }
    }

    public string MathOperator
    {
        get
        {
            return mathOperator;
        }

        set
        {
            mathOperator = value;
            transform.GetChild(1).GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprite/Operator/"+mathOperator);

        }
    }
    // Use this for initialization
    void Start () {
        gamePlay = GameObject.Find("GamePlay");
        Items = GameObject.Find("Items");
        coinButton = GetComponent<Button>();
        coinButton.onClick.AddListener(() => actionToCalculate());
	}

    int GetMax(List<int> data)
    {
        int max = 0;
        for(int i = 0; i<data.Count; i++)
        {
            if (data[i] >= max)
                max = data[i];
        }

        return max;
    }

    void actionToCalculate()
    {
        
        int currentCoin, newCurrentCoin;
        currentCoin = gamePlay.GetComponent<GamePlay>().PlayerCoinValue;
        clickAudio = GetComponent<AudioSource>();
        clickAudio.Play();

        if (mathOperator == "Plus")
            newCurrentCoin = currentCoin + mathValue;
        else if (mathOperator == "Minus")
            newCurrentCoin = currentCoin - mathValue;
        else if (mathOperator == "Multiple")
            newCurrentCoin = currentCoin * mathValue;
        else
            newCurrentCoin = currentCoin / mathValue;
        
        if (newCurrentCoin > GetMax(gamePlay.GetComponent<GamePlay>().items) || newCurrentCoin < 0)
        {
            gamePlay.GetComponent<GamePlay>().PlayerCoinValue = 0;
        } else if (gamePlay.GetComponent<GamePlay>().items.Contains(newCurrentCoin)) {
            
            gamePlay.GetComponent<GamePlay>().PlayerCoinValue = 0;

            GameObject itemSelected = gamePlay.GetComponent<GamePlay>().gameObjectItems.Find(item => item.GetComponent<Item>().Price == newCurrentCoin);
            int indexItemSelected = gamePlay.GetComponent<GamePlay>().gameObjectItems.FindIndex(item => item.GetComponent<Item>().Price == newCurrentCoin);

            gamePlay.GetComponent<GamePlay>().createPlayerAttack(itemSelected.GetComponent<Image>().sprite.name, itemSelected.GetComponent<Item>().Price);
            Items.GetComponent<PopulateItem>().createItem();
            gamePlay.GetComponent<GamePlay>().gameObjectItems.Remove(itemSelected);
            gamePlay.GetComponent<GamePlay>().items.RemoveAt(indexItemSelected);
            Destroy(itemSelected);

        } else
        {
            gamePlay.GetComponent<GamePlay>().PlayerCoinValue = newCurrentCoin;
            string[] Operator = new string[] { "Plus", "Plus", "Plus","Minus", "Plus","Minus","Multiple", "Multiple", "Plus" };

            this.transform.parent.gameObject.GetComponent<PopulateCoin>().createCoin(this.transform.GetSiblingIndex(), Operator[Random.Range(0, level.jenisOperator[PlayerPrefs.GetInt("jenisLevel"), PlayerPrefs.GetInt("levelPlay")])]);

            Destroy(this.gameObject);
        }
        
    }

    // Update is called once per frame
    void Update () {
	
	}
}
